/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PorocilaService } from './porocila.service';

describe('Service: Porocila', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PorocilaService]
    });
  });

  it('should ...', inject([PorocilaService], (service: PorocilaService) => {
    expect(service).toBeTruthy();
  }));
});
