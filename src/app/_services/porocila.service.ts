import { Porocilo } from './../_models/porocilo';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PorocilaService {
  private porocila: Porocilo[] = [
    {id: 1, imeStranke: 'Janez', priimekStranke: 'Novak', lokacijaNesrece: 'Ljubljana', vzrokNesrece: 'Divjad na cesti', idIzvajalca: 2, imeIzvajalca: 'Triglav', opravljenost: false},
    {id: 2, imeStranke: 'David', priimekStranke: 'Kranjc', lokacijaNesrece: 'Maribor', vzrokNesrece: 'Poledica', idIzvajalca: 3, imeIzvajalca: 'Sava', opravljenost: false},
    {id: 3, imeStranke: 'Luka', priimekStranke: 'Mlakar', lokacijaNesrece: 'Kranj', vzrokNesrece: 'Prevelika hitrost', idIzvajalca: 4, imeIzvajalca: 'Adriatic', opravljenost: false},
    {id: 4, imeStranke: 'Ana', priimekStranke: 'Kralj', lokacijaNesrece: 'Koper', vzrokNesrece: 'Poledica', idIzvajalca: 3, imeIzvajalca: 'Sava', opravljenost: false},
    {id: 5, imeStranke: 'Gregor', priimekStranke: 'Petek', lokacijaNesrece: 'Ajdovščina', vzrokNesrece: 'Slaba cesta', idIzvajalca: 2, imeIzvajalca: 'Triglav', opravljenost: false},
    {id: 6, imeStranke: 'Nina', priimekStranke: 'Oblak', lokacijaNesrece: 'Murska Sobota', vzrokNesrece: 'Psihofizično stanje voznika', idIzvajalca: 4, imeIzvajalca: 'Adriatic', opravljenost: false},
    {id: 7, imeStranke: 'Tina', priimekStranke: 'Kos', lokacijaNesrece: 'Celje', vzrokNesrece: 'Napačna smer vožnje', idIzvajalca: 3, imeIzvajalca: 'Sava', opravljenost: false},
  ];

  private porocilaUpdated = new Subject<Porocilo[]>();


  constructor() {}

  getPorocila() {
    return [...this.porocila];
  }

  getPorocilaUpdatedListener() {
    return this.porocilaUpdated.asObservable();
  }

  getPorocilaById(id: number): Porocilo[] {
    const filterPorocila: Porocilo[] = this.porocila.filter(p => p.idIzvajalca == id);
    return filterPorocila;
  }

  dodajPorocilo(porocilo: Porocilo) {
    porocilo['id'] = this.porocila.length + 1;
    this.porocila.push(porocilo);
    this.porocilaUpdated.next([...this.porocila]);
  }

  updatePorocilo(id: number, opravljenost: boolean) {
    const porocilo = this.porocila.find(p => p.id === id);
    if (porocilo) {
      const index = this.porocila.indexOf(porocilo);
      this.porocila[index]['opravljenost'] = opravljenost;
    }
    this.porocilaUpdated.next([...this.porocila]);
  }
}
