import { User } from './../_models/users';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private users: User[] = [
    {id: 1, ime: 'Operater', tipUporabnika: 'operater'},
    {id: 2, ime: 'Triglav', tipUporabnika: 'izvajalec'},
    {id: 3, ime: 'Sava', tipUporabnika: 'izvajalec'},
    {id: 4, ime: 'Adriatic', tipUporabnika: 'izvajalec'}
  ];

  selectedUserUpdated = new Subject<User>();
  selectedUser: User;


  constructor(private router: Router) {
   }

  getIzvajalci() {
    return [...this.users.filter(u => u.tipUporabnika === 'izvajalec')];
  }

  getUsers() {
    return [...this.users];
  }

  selectedUserListener() {
    return this.selectedUserUpdated.asObservable();
  }

  setSelectedUser(user: User) {
    localStorage.setItem('selectedUser', String(user.id));
    this.selectedUser = user;
    this.selectedUserUpdated.next(user);
    this.router.navigate(['/']);
  }

  getSelectedUser() {
    const id = +localStorage.getItem('selectedUser');
    //console.log(id);
    if (id && id !== 0) {
      return this.users.find(u => u.id === id);
    }
    return this.users.find(u => u.id === 1);
  }

  isUserAuthorised() {
    const user = this.getSelectedUser();
    //console.log(user);
    if (user && user.tipUporabnika === 'operater') {
      return true;
    }

    return false;
  }

}
