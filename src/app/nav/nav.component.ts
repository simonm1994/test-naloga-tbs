import { User } from './../_models/users';
import { UsersService } from 'src/app/_services/users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  users: User[];
  selectedUser: User;

  constructor(public userService: UsersService) { }

  ngOnInit() {
    this.users = this.userService.getUsers();
    this.selectedUser = this.userService.getSelectedUser();
  }

  changeUser(user: User) {
    this.userService.setSelectedUser(user);
    this.selectedUser = user;
  }

}
