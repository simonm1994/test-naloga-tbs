import { User } from './../../_models/users';
import { PorocilaService } from './../../_services/porocila.service';
import { Porocilo } from './../../_models/porocilo';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/_services/users.service';

@Component({
  selector: 'app-create-report',
  templateUrl: './create-report.component.html',
  styleUrls: ['./create-report.component.css']
})
export class CreateReportComponent implements OnInit {
  izvajalci: User[];

  constructor(private porocilaService: PorocilaService, private usersService: UsersService, private router: Router) { }

  ngOnInit() {
    this.izvajalci = this.usersService.getIzvajalci();
  }

  onSubmit(form: NgForm) {
    const porocilo: Porocilo = {
      id: null,
      imeStranke: form.value.userData.ime,
      priimekStranke: form.value.userData.priimek,
      lokacijaNesrece: form.value.accidentData.lokacija,
      vzrokNesrece: form.value.accidentData.vzrok,
      idIzvajalca: form.value.izvajalec,
      imeIzvajalca: this.izvajalci.find(i => i.id == form.value.izvajalec).ime,
      opravljenost: false
    };

    this.porocilaService.dodajPorocilo(porocilo);

    this.router.navigate(['/']);
  }

}
