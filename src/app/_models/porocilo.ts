export interface Porocilo {
  id: number;
  imeStranke: string;
  priimekStranke: string;
  lokacijaNesrece: string;
  vzrokNesrece: string;
  idIzvajalca: number;
  imeIzvajalca: string;
  opravljenost: boolean;
}
