import { AuthGuard } from './_guards/auth.guard';
import { CreateReportComponent } from './operater/create-report/create-report.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportListComponent } from './report-list/report-list.component';

const routes: Routes = [
  {path: '', component: ReportListComponent},
  {path: 'create', component: CreateReportComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
