import { AuthGuard } from './_guards/auth.guard';
import { CreateReportComponent } from './operater/create-report/create-report.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap';
import { UiSwitchModule } from 'ngx-toggle-switch';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReportListComponent } from './report-list/report-list.component';
import { NavComponent } from './nav/nav.component';

@NgModule({
   declarations: [
      AppComponent,
      CreateReportComponent,
      ReportListComponent,
      NavComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      BsDropdownModule.forRoot(),
      UiSwitchModule
   ],
   providers: [
     AuthGuard
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
