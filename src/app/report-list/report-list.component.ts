import { UsersService } from './../_services/users.service';
import { PorocilaService } from './../_services/porocila.service';
import { Porocilo } from './../_models/porocilo';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/_models/users';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css']
})
export class ReportListComponent implements OnInit, OnDestroy {
  columns = ['#', 'Ime', 'Priimek', 'Lokacija', 'Vzrok'];
  porocila: Porocilo[];
  selectedUser: User;
  filter = 'vse';
  private porocilaSubscription: Subscription;
  private userSubscription: Subscription;

  constructor(private porocilaService: PorocilaService, public usersService: UsersService) { }

  ngOnInit() {
    this.selectedUser = this.usersService.getSelectedUser();
    this.getPorocila();
    this.porocilaSubscription = this.porocilaService.getPorocilaUpdatedListener()
      .subscribe((data) => {
        this.getPorocila();
      });
    this.userSubscription = this.usersService.selectedUserListener()
      .subscribe((data: User) => {
        this.selectedUser = data;
        this.getPorocila();
      });
  }

  getPorocila() {
    if (this.selectedUser.tipUporabnika === 'operater') {
      switch (this.filter) {
        case 'vse':
          this.porocila = this.porocilaService.getPorocila();
          break;
        case 'opravljene':
          this.porocila = this.porocilaService.getPorocila().filter(p => p.opravljenost == true);
          break;
        case 'neopravljene':
          this.porocila = this.porocilaService.getPorocila().filter(p => p.opravljenost == false);
          break;
        default:
        this.porocila = this.porocilaService.getPorocila();
          break;
      }
    } else {
      this.porocila = this.porocilaService.getPorocilaById(this.selectedUser.id);
    }
    console.log(this.porocila);
  }

  isAuthenticated() {
    const auth = this.usersService.isUserAuthorised();
    return auth;
  }

  updatePorocilo(id: number, opravljenost: boolean) {
    this.porocilaService.updatePorocilo(id, opravljenost);
  }

  ngOnDestroy() {
    this.porocilaSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }




}
