import { UsersService } from './../_services/users.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor (private usersService: UsersService, private router: Router) {}

  canActivate(): boolean {
    console.log(this.usersService.isUserAuthorised());
    if (this.usersService.isUserAuthorised()) {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
